.. include:: images.rst

.. _valuebuilder-label:

Cataloguing plugins (value builders)
===============================================================================

With value builders you can enhance your cataloguing with additional features 
like validation, link creation, auto-generation of numbers etc.

A value builder is a perl script that, within a MARC framework, is linked to a 
certain subfield in bibliographic records, authority records or items. 

.. Note::

   Cataloguing plugins are not to be confused with 
   :ref:`Koha plugins <plugin-system-label>`

.. _setup-valuebuilder-label:

Set up
-------------------------------------------------------------------------------

*Get there:* Administration > MARC bibliographic framework

To use a value builder in your MARC framework, navigate to Administration > 
MARC bibliographic framework and then to the MARC structure of the desired 
framework.

Search for or browse to a field, and choose 'Edit subfields' from its 'Actions' 
menu. Go to the subfield's tab, and choose the relevant entry from the drop down
list at "plugin".

Example
-------------------------------------------------------------------------------

Adding the call number browser to the items editor:

#. In your MARC framework, go to field 952 and edit subfield o.
#. Choose cn\_browser.pl from the 'Plugin' list.
#. Save your changes.

|image1518|

.. _default-marc21-value-builders-label:

Default value builders for MARC21
-------------------------------------------------------------------------------

These value builders are already integrated to the default MARC bibliographic
framework in MARC21.

To access a value builder from the
:ref:`basic editor <basic-editor-cataloging-label>`, click the icon on the right
of the field.

.. _marc21-biblio-leader-value-builder-label:

Leader (bibliographic record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The leader value builder is linked to the 000 MARC field in all the default
MARC21 bibliographic frameworks.

|leaderbuilder|

The values in this builder are based on the
`leader values in MARC21 (bibliographic) <https://loc.gov/marc/bibliographic/bdleader.html>`_.

-  00-04 - Record size: this will be filled automatically by Koha.

-  05 - Record status: in new records, this will be set to 'n - New' by default.

-  06 - Type of record: in new records, this will be set to 'a - Language
   material' by default.

-  07 - Bibliographic level: in new records, this will be set to 'm - Monograph/
   item' by default. Some features in Koha, such as
   :ref:`item bundles <bundles-circulation-label>`,
   :ref:`analytics <adding-analytic-records-label>` and
   :ref:`CheckPrevCheckout <checkprevcheckout-label>` might depend on having a
   different value here.

-  08 - Type of control: in new records, this will be set to '# - No specific
   type' by default.

-  09 - Character coding scheme: records in Koha are always in Unicode.

-  10-16 - Indicator/subfields/size: this will be filled automatically by Koha.

-  17 - Encoding level: in new records, this will be set to '7 - Minimal level'
   by default.

-  18 - Descriptive cataloging form: in new records, this will be set to 'a - 
   AACR 2' by default.

-  19 - Multipart resource record level: in new records, this will be set to
   '# - Not specified or not applicable' by default.

-  20-24 - Entry map & lengths: this will be filled automatically by Koha.

.. _marc21-authority-leader-value-builder-label:

Leader (authority record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The leader value builder is linked to the 000 MARC field in all the default
MARC21 authority frameworks.

|leaderbuilderauthority|

The values in this builder are based on the
`leader values in MARC21 (authority) <https://www.loc.gov/marc/authority/adleader.html>`_.

-  00-04 - Record size: this will be filled automatically by Koha.

-  05 - Record status: in new records, this will be set to 'n - New' by default.

-  06 - Type of record: this will be set to 'z - Authority data'.

-  07-08 - Undefined character positions

-  09 - Character coding scheme: in new records, this will be set to 'a - UCS/
   Unicode' by default.

-  10-16 - Indicator/subfields/size: this will be filled automatically by Koha.

-  17 - Encoding level: in new records, this will be set to 'n - Complete
   authority record' by default.

-  18-19 - Undefined character positions

-  20-24 - Entry map & lengths: this will be filled automatically by Koha.

.. _marc21-biblio-006-value-builder-label:

006 (bibliographic record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The 006 value builder is linked to the 006 MARC field in all the default MARC21
bibliographic frameworks.

|006builder|

The values in this builder are based on the
`006 field values in MARC21 <https://www.loc.gov/marc/bibliographic/bd006.html>`_.

-  Type of material: this value will change the fields and options below, since
   elements in 006 are defined by type of material. In new records, this will
   be set to 'BKS - Books' by default.

-  00 - Form of material: the default value and options here will depend on the
   type of material above.

The rest of the values are the same as 008/18-34, see the description below.

.. _marc21-biblio-007-value-builder-label:

007 (bibliographic record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The 007 value builder is linked to the 007 MARC field in all the default MARC21
bibliographic frameworks.

|007builder|

The values in this builder are based on the
`007 field values in MARC21 <https://www.loc.gov/marc/bibliographic/bd007.html>`_.

-  Material type: this value will change the fields and options below, since
   elements in 007 are defined by type of material. In new records, this will
   be set to 'Text' by default.

.. _marc21-biblio-008-value-builder-label:

008 (bibliographic record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The 008 value builder is linked to the 008 MARC field in all the default MARC21
bibliographic frameworks.

|008plugin|

The values in this builder are based on the
`008 field values in MARC21 (bibliographic) <https://www.loc.gov/marc/bibliographic/bd008.html>`_.

-  Type of material: this value will change the fields and options below, since
   elements in 008 are defined by type of material. The default value depends
   on the value in leader/06 (as well as the value in leader/07 in some cases).

   +--------------------------------------------------------------+--------------------------------+---------------------------+
   | leader/06 value                                              | leader/07 value                | 008 Type of material      |
   +==============================================================+================================+===========================+
   | a - Language material                                        | a - Monographic component part | BKS - Books               |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | b - Serial component part      | CR - Continuing resources |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | c - Collection                 | BKS - Books               |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | d - Subunit                    | BKS - Books               |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | i - Integrating resource       | CR - Continuing resources |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | m - Monograph/Item             | BKS - Books               |
   |                                                              +--------------------------------+---------------------------+
   |                                                              | s - Serial                     | CR - Continuing resources |
   +--------------------------------------------------------------+--------------------------------+---------------------------+
   | c - Notated music                                                                             | MU - Music                |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | d - Manuscript notated music                                                                  | MU - Music                |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | e - Cartographic material                                                                     | MP - Maps                 |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | f - Manuscript cartographic material                                                          | MP - Maps                 |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | g - Projected medium                                                                          | VM - Visual materials     |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | i - Nonmusical sound recording                                                                | MU - Music                |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | j - Musical sound recording                                                                   | MU - Music                |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | k - Two-dimensional nonprojectable graphic                                                    | VM - Visual materials     |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | m - Computer file                                                                             | CF - Computer files       |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | o - Kit                                                                                       | VM - Visual materials     |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | p - Mixed materials                                                                           | MX - Mixed materials      |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | r - Three-dimensional artifact or naturally occurring object                                  | VM - Visual materials     |
   +-----------------------------------------------------------------------------------------------+---------------------------+
   | t - Manuscript language material                                                              | BKS - Books               |
   +-----------------------------------------------------------------------------------------------+---------------------------+

.. _marc21-authority-008-value-builder-label:

008 (authority record)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The 008 value builder is linked to the 008 MARC field in all the default MARC21
authority frameworks.

|008builderauthority|

The values in this builder are based on the
`008 field values in MARC21 (authority) <https://www.loc.gov/marc/authority/ad008.html>`_.

.. I commented the UNIMARC section since it is not yet documented. CCLR 2023-12-14

.. .. _default-unimarc-value-builders-label:

.. Default value builders for UNIMARC
.. -------------------------------------------------------------------------------

.. _other-default-value-builders-label:

Other default value builders
-------------------------------------------------------------------------------

As well as the default value builders for specific MARC fields, there are
other cataloging plugins that are linked to fields by default in Koha.

.. _barcode-value-builder-label:

Barcode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This value builder is the plugin linked to the barcode field in all default
MARC bibliographic frameworks (
:ref:`952$p in MARC21 or 995$f in UNIMARC <952p-barcode-label>`) by default.

It is used by the :ref:`autoBarcode <autobarcode-label>` system preference. If
the preference is set to a barcode format, the barcode value builder will
generate a barcode automatically when the user clicks in the barcode field.

You can alternatively use the optional
:ref:`manual barcode value builder <barcode-manual-value-builder-label>`
instead of this default one.

.. _dateaccessioned-value-builder-label:

Date of acquisition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This value builder is the plugin linked to the date of acquisition item field
in all default MARC bibliographic frameworks (
:ref:`952$d in MARC21 or 995$5 in UNIMARC <952d-date-acquired-label>`) by
default.

This simply enters today's date when the user clicks in the field.

.. _optional-value-builders-label:

Optional value builders
-------------------------------------------------------------------------------

As well as the default value builders for specific MARC fields or other fields,
there are other cataloging plugins that can optionally be
:ref:`added to MARC frameworks <setup-valuebuilder-label>` to help with various
tasks when cataloging records.

.. _barcode-manual-value-builder-label:

Manual barcode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This value builder is an optional alternative to the
:ref:`Barcode value builder <barcode-value-builder-label>`. It does the same
thing, but the barcode is only generated when the user clicks on the ellipsis
(...) next to the barcode field.

It can be used in libraries that don't necessarily want a barcode generated
automatically for all items.

To use this value builder instead of the regular barcode value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the barcode tab ('p' in MARC21, or 'f' in UNIMARC)

-  Under 'Other options' choose 'barcode\_manual.pl' as the plugin

-  Click 'Save changes'

.. _callnumber-ku-value-builder-label:

Call number (sequential, 4 characters with prefix)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This value builder can optionally be used to create in-house sequential
alphanumeric call numbers.

You must supply a letter prefix (of any length), followed by an empty
space and an alphanumeric number. The alphanumeric number is 4 characters long,
and is either a letter or number sequence, which is then appended by 1, 2, 3,
etc.

If the input is not in this exact format, nothing will be generated.

Here are some examples of values that will trigger a call number generation:

-  :code:`AAA 0` returns first unused number AAA 0xxx starting with AAA 0001

-  :code:`BBB 12` returns first unused number BBB 12xx starting with BBB 1201

-  :code:`CCC QW` returns first unused number CCC QWxx starting with CCC QW01

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the call number tab ('o' in MARC21, or 'k' in UNIMARC)

-  Under 'Other options' choose 'callnumber-KU.pl' as the plugin

-  Click 'Save changes' 

To generate a call number, type the prefix, space and start of the sequential
number in the call number field, then click the ellipsis (...) next to the field.
The call number will be generated according to the pattern supplied.

.. _callnumber-value-builder-label:

Call number (sequential digits with prefix)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This value builder can optionally be used to create in-house sequential
numeric call numbers, with or without a letter prefix.

You must supply a letter prefix (of any length). The plugin will append
an incremented number to the prefix.

For example, if the highest call number with the prefix "PREFIX" is "PREFIX 5236",
the next "PREFIX" call number to be generated will be "PREFIX 5237".

If you leave the field empty, a simple incremented call number is generated.

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the call number tab ('o' in MARC21, or 'k' in UNIMARC)

-  Under 'Other options' choose 'callnumber.pl' as the plugin

-  Click 'Save changes' 

To generate a call number, type the prefix, then click the ellipsis (...) next
to the field. The call number will be generated according to the pattern supplied.

.. _cnbrowser-value-builder-label:

Callnumber browser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This plugin can be optionally used to browse the call numbers directly from the
item editor.

Clicking the ellipsis (...) next to the call number field will open a pop-up
window with a search form.

|cnbrowserempty|

If you type in a call number and click the ellipsis, the search form
will be prefilled with the call number.

In the results, a thin green line indicates where the searched call number would
be in the sequence.

|cnbrowser|

If the call number exists, the results will show in red.

|cnbrowsermatch|

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the call number tab ('o' in MARC21, or 'k' in UNIMARC)

-  Under 'Other options' choose 'cn\_browser.pl' as the plugin

-  Click 'Save changes' 

.. _stocknumber-value-builder-label:

Stocknumber (library code)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This plugin can optionally be used to generate an incremented inventory number,
prefixed by the library code.

For example, if the library code is CPL, the stocknumbers will be generated as
CPL\_1, CPL\_2, CPL\_3, etc.

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the inventory number tab ('i' in MARC21, or 'j' in UNIMARC)

-  Under 'Other options' choose 'stocknumber.pl' as the plugin

-  Click 'Save changes'

When cataloging an item, click the ellipsis (...) next to the inventory number
field. If there are no inventory numbers yet, only the prefix will be generated
(CPL\_ for example). If there are already inventory numbers, it will take the
largest one and add one increment.

.. _stocknumberam123-value-builder-label:

Stocknumber (10 digits with prefix)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This plugin can optionally be used to generate a 10-digit incremented inventory
number, with a custom prefix.

For example, if the last inventory number is 'CAT 0000001456' the next 'CAT'
inventory number will be 'CAT 0000001457'.

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the inventory number tab ('i' in MARC21, or 'j' in UNIMARC)

-  Under 'Other options' choose 'stocknumberam123.pl' as the plugin

-  Click 'Save changes'

When cataloging an item, type the prefix and click the ellipsis (...) next to
the inventory number field. It will take the largest number with the same prefix
and add one increment.

.. _stocknumberav-value-builder-label:

Stocknumber (10 digits with pre-determined prefix)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This plugin can optionally be used to generate a 10-digit incremented inventory
number, with a custom prefix from an authorized value category.

For example, if the last inventory number is 'CAT 0000001456' the next 'CAT'
inventory number will be 'CAT 0000001457'.

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the item field (952 in MARC21, 995
   in UNIMARC)

-  Click the inventory number tab ('i' in MARC21, or 'j' in UNIMARC)

-  Under 'Other options' choose 'stocknumberAV.pl' as the plugin

-  Click 'Save changes'

In order to use this value builder, you need to
:ref:`create an authorized value category <add-new-authorized-value-category-label>`
called 'INVENTORY'.

Then, :ref:`add authorized values <add-new-authorized-value-label>` like so:

-  Authorized value: enter the prefix you want for your inventory numbers
   (uppercase or lowercase).

-  Description: enter the first value for your number. For example, if your
   prefix is 'CAT' and you want your first inventory number to be 'CAT
   0000001457', enter '1456' or '0000001456' in the authorized value description.

-  Description (OPAC): leave empty.

When cataloging an item, type the prefix and click the ellipsis (...) next to
the inventory number field. It will take the largest number with the same prefix
and add one increment. If you type an invalid prefix, it will not generate any
number.

.. _upload-value-builder-label:

Upload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This plugin can optionally be used to upload files (with the
:ref:`upload tool <upload-label>`) directly from the cataloging editor and
automatically link the file in the field. This is useful to
:ref:`attach files <attaching-files-to-records-label>` to records (most commonly
used with the $u subfield of the 'electronic location and access' field - 856 in
MARC21 or 256 in UNIMARC).

To use this value builder,

-  Go to :ref:`Administration > MARC bibliographic frameworks <marc-bibliographic-frameworks-label>`

-  Click Actions > MARC structure next to the framework to edit

-  Click Actions > Edit subfields next to the link field (856 in MARC21, 256
   in UNIMARC)

-  Click the 'u' tab

-  Under 'Other options' choose 'upload.pl' as the plugin

-  Click 'Save changes'

To attach files to records when cataloging,

-  Click the 'Upload' button next to the $u subfield.

   |856uupload|

-  Click the button to choose a file from your computer.

   |uploadfilebib|

-  Click 'Upload'.

-  Click 'Choose'.

   |uploadfilechoose|

The link to the file will be automatically entered in the subfield.
